# Ordering Dashboard API

Simple serverless API that uses AWS Lambda to interact with DynamoDB

Project use Serverless Framework to orchestrate the AWS services: API Gateway, Lambda, DynamoDB

## Usage

### GET /queue

Fetches all of the items stored in DynamoDB

LIVE URL: https://dwj8qv9jo5.execute-api.us-east-1.amazonaws.com/dev/queue

### POST /queue

Adds data to the the DynamoDB table from the request json

LIVE URL: https://dwj8qv9jo5.execute-api.us-east-1.amazonaws.com/dev/queue

Expected payload:

```json
{
  "Product_Name": "Nike Air VaporMax Flyknit 3",
  "Category": "Running",
  "Size": "8.5",
  "Color": "black",
  "Status": "green",
  "Customer_initials": "AI",
  "Product_Thumbnail": "https://via.placeholder.com"
}
```

Predicted output:

```json
[
  {
    "Product_Thumbnail": "https://via.placeholder.com",
    "updatedAt": 1585507610112,
    "Size": "8.5",
    "createdAt": 1585507610112,
    "Product_Name": "Nike Air VaporMax Flyknit 3",
    "checked": false,
    "Status": "green",
    "Color": "black",
    "id": "a6662000-71ed-11ea-839a-6babc5f40d18",
    "Customer_initials": "AI",
    "Category": "Running"
  }
]
```

### DELETE /queue/{id}

Removes item from DynamoDB based on the id paramater

LIVE URL : https://dwj8qv9jo5.execute-api.us-east-1.amazonaws.com/dev/queue/

Predicted response: Status 200, with an empty response

## Install

Currently not setup for local DynamoDB development

## Acknowledgments

ordering-dasboard-api was inspired by..[Serverless Node REST API with Dynamo](https://github.com/serverless/examples/tree/master/aws-node-rest-api-with-dynamodb)

## See Also

- [`noffle/common-readme`](https://github.com/noffle/common-readme)
- ...

## License

ISC
